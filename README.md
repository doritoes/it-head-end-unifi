# IT Head End - Unifi

Specifications for an IT Head End based on Unifi

# Objective

Create a somewhat "complete" IT head end to brings a managed IT infrastructure 
to a site. 

# Description

Roll a robust "IT Head End" consisting of:

*  Durable travel case with integral 4-6U rack
    *  Able to be shipped with optional lock(s)
    *  Prefer handle and wheel
*  Unifi ntework equipment
    *  Unifi switch with PoE to power access points (rack mount)
    *  Unifi USG PRO (rack mount)
    *  Unifi second generation CloudKey (rack mount)
*  Power conditioner with optional small rack-mount UPS
*  Patch panels front and rear for each connection of
    *  Internet connections
    *  Gig laptops
    *  Access points
    *  Video
        *  Monitors
        *  SDI/HDMI capture input
        *  SDI/HDMI/Display Port output
        *  Optional BlackMagic web presenter or other video capture device
*  Compute
    * 1-2U of compute with integral storage
    * 2 x 1U servers or 2U rack mounted 3 NUC devices
*  Cooling
    *  use available space to install fans for cooling
* Storage
    *  Use limited remaining space to store keyboard, mouse, cables for shipping

Sofware capabilities will include:

* Unifi controller
* VPN for remote access
* Linux server infrastructure
* Optional Windows workstation
* OBS video capture
* Self-monitoring
