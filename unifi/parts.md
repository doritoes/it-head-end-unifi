# Parts List
This parts list is covers all Unifi SKUs and parts specified for the project.

Status: Early design. Not complete.

# Basic
Used separate USG Pro, Cloud Key, switch

## Network Equipment
| Quantity  | Part Number  | Description  | Purpose  |
|---|---|---|---|
| 1  | USG-PRO-4  | Secure Gateway Pro  | Firewall and remote access  |
| 1  | UCK-G2-PLUS  | Cloud Key Gen2 Plus | Manage the Unifi network components  |
 | 1  | US‑16‑150W  | Unifi PoE 150W switch<sup>[1](#myfootnote1)</sup>  | Switching  |
| 1  | UF-MM-10G SFP+  | 10G SFP+ Multi-Mode Fiber Module (2 pack)  | Connect USG to switch  |

<a name="myfootnote1">1</a>: The switch selection can be easily changed. The Gen 2 series switches are ideal.

## Other Parts
| Quantity  | Part Number  | Description  | Purpose  |
|---|---|---|---|
| 1  | CKG2-RM  | Rack mount for CloudKey Gen2  | Rack mount the CloudKey network controller  |

# Advanced
Space-saving design with combined functions. Second generation switch for high 
performance. Can power the XG access points

## Network Equipment
| Quantity  | Part Number  | Description  | Purpose  |
|---|---|---|---|
| 1  | UDM-Pro | Dream Machine Pro  | Includes USG, basic switch and  cloud key on one unit  |
| 1  | USW-Pro-24-POE | Switch Pro 24 port  | 16x PoE+, 8x PoE++,  2x 10 GB SFP+  |
| 1  | UF-MM-10G SFP+  | 10G SFP+ Multi-Mode Fiber Module (2 pack)  | Connect Dream Machine to switch  |

## Other Parts

